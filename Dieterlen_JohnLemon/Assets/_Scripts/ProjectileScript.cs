﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.isTrigger)
        {
            if (other.gameObject.CompareTag("enemy"))
            {
                EnemyHealth eHeatlh = other.gameObject.GetComponent<EnemyHealth>();

                if (eHeatlh != null)
                    eHeatlh.TakeDamage(1);
            }

            Destroy(gameObject);
        }

    }
}
