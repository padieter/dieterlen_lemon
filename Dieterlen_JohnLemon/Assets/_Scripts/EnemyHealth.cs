﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public int health = 3;
    static int count;
    public Text countText;
    public GameEnding gameEnding;

    public void Start()
    {
        count = 7;
        SetCountText();
    }

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;

        if (health <= 0)
        {
            Destroy(gameObject);
            count = count - 1;
            SetCountText();
            if (count == 0)
            {
                gameEnding.m_IsPlayerAtExit = true;
            }
        }
    }

    void SetCountText()
    {
        countText.text = "Bad Guys: " + count.ToString();
    }
}
