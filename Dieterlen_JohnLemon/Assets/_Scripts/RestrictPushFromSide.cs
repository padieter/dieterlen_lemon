﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestrictPushFromSide : MonoBehaviour
{

    public enum Sides {Left, Up, Right, Down};

    public Sides sideToPush;

    GameObject player;

    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            rb.isKinematic = true;

            if (sideToPush == Sides.Left && player.transform.position.x < transform.position.x)
            {
                rb.isKinematic = false;
            }
            else
                if (sideToPush == Sides.Right && player.transform.position.x < transform.position.x)
            {
                rb.isKinematic = false;
            }
            else
                if(sideToPush == Sides.Up && player.transform.position.z < transform.position.z)
            {
                rb.isKinematic = false;
            }
            else
                if(sideToPush == Sides.Down && player.transform.position.z < transform.position.z)
            {
                rb.isKinematic = false;
            }
        }
    }
}
